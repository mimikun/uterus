package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(SyuponCmd)
}

var SyuponCmd = &cobra.Command{
	Use:   "syupon",
	Short: "ｵｷﾞｬｬｬｧｧｧｧｧｯｼｭﾙﾙﾙﾙﾙﾙｼｭﾎﾟﾝｯ（還る音）",
	Run: func(cmd *cobra.Command, args []string) {
		if len(os.Args) <= 2 {
			fmt.Println("ｵｷﾞｬｬｬｧｧｧｧｧｯｼｭﾙﾙﾙﾙﾙﾙｼｭﾎﾟﾝｯ（子宮に還る音）")
			os.Exit(1)
		}
		uterusName := args[0]
		fmt.Printf("ｵｷﾞｬｬｬｧｧｧｧｧｯｼｭﾙﾙﾙﾙﾙﾙｼｭﾎﾟﾝｯ（%sの子宮に還る音）\n", uterusName)
	},
}
