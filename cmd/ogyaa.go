package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

func init() {
	RootCmd.AddCommand(OgyaaCmd)
}

var OgyaaCmd = &cobra.Command{
	Use:   "ogyaa",
	Short: "ｵｷﾞｬｬｬｧｧｧｧｧｯ（出る音）",
	Run: func(cmd *cobra.Command, args []string) {
		if len(os.Args) <= 2 {
			fmt.Println("ｵｷﾞｬｬｬｧｧｧｧｧｯ（子宮から出る音）")
			os.Exit(1)
		}
		uterusName := args[0]
		fmt.Printf("ｵｷﾞｬｬｬｧｧｧｧｧｯ（%sの子宮から出る音）\n", uterusName)
	},
}
