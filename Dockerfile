FROM golang:alpine

RUN apk add --no-cache git

RUN go get -u bitbucket.org/mimikun/uterus
