package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	cobra.OnInitialize()
	RootCmd.AddCommand(VersionCmd)
}

// RootCmd is RootCmd
var RootCmd = &cobra.Command{
	Use:   "uterus",
	Short: "ogyasyuru command",
	Run: func(cmd *cobra.Command, args []string) {

	},
}

var VersionCmd = &cobra.Command{
	Use:   "version",
	Short: "show version",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("uterus v1.0")
	},
}
