# 還ったり出たりするコマンド
**これはジョークソフトです。使用は自己責任でお願いいたします。**

## 必要なもの
Goの環境

## インストール
```
go get bitbucket.org/mimikun/uterus
```

## 使い方

```
uterus help         ヘルプ
uterus ogyaa <uterus_name>         出る
> ｵｷﾞｬｬｬｧｧｧｧｧｯ（uterus_nameの子宮から出る音）
uterus syupon <uterus_name>         還る
> ｵｷﾞｬｬｬｧｧｧｧｧｯｼｭﾙﾙﾙﾙﾙﾙｼｭﾎﾟﾝｯ（uterus_nameの子宮に還る音）
```


## ToDo
- [ ] テストを書く
