package main

import (
	"fmt"
	"os"

	"bitbucket.org/mimikun/uterus/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
